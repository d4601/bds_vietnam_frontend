import axios from 'axios'
import env from 'env'
// import { getDeviceId } from 'utils/deviceUUID'
import { getToken } from 'utils/auth'
import { i18n } from 'utils/i18nextUtils'
import queryString from 'query-string'

global.Authorization = ''
global.AppLocale = ''
global.AppDeviceId = ''

export const setGlobalAuthorization = (token) => {
  // console.log('setGlobalAppLocale')
  global.Authorization = token
}

export const setGlobalAppLocale = (appLocale) => {
  // console.log('setGlobalAppLocale')
  global.AppLocale = appLocale
}

export const setGlobalAppDeviceId = (appDeviceId) => {
  // console.log('setGlobalAppDeviceId')
  global.AppDeviceId = appDeviceId
}

const axiosClient = axios.create({
  baseURL: env.default.API_URL,
  headers: {
    'content-type': 'application/json'
  },
  paramsSerializer: (params) => queryString.stringify(params)
})

const getAuthorization = () => {
  const currentToken = process.browser ? getToken() : ''
  if (currentToken) setGlobalAuthorization(currentToken)

  return global.Authorization
}

const getAppLocale = () => {
  const currentLanguage = process.browser ? i18n.language : ''
  if (currentLanguage) setGlobalAppLocale(currentLanguage)

  return global.AppLocale
}

// const getAppDeviceId = () => {
//   const currentAppDeviceId = process.browser ? getDeviceId() : ''
//   if (currentAppDeviceId) setGlobalAppDeviceId(currentAppDeviceId)

//   return global.AppDeviceId
// }

axiosClient.interceptors.request.use(async (config) => {
  const Authorization = getAuthorization()
  if (Authorization) {
    config.headers.Authorization = Authorization
  }

  config.headers.AppName = 'customer'
  config.headers.AppPlatform = 'Web'
  config.headers.AppVersion = '2.0.0'
  // config.headers.AppDeviceId = getAppDeviceId()
  config.headers.AppLocale = getAppLocale()

  // console.log('config.headers', config.headers)

  return config
})

axiosClient.interceptors.response.use((response) => {
  return {
    success: true,
    data: response?.data ?? response
  }
}, (error) => {
  return {
    success: false,
    data: error?.response?.data ?? error
  }
})

export default axiosClient
