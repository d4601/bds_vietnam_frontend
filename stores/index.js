import { MobXProviderContext, useStaticRendering } from 'mobx-react'
import authStore from './authStore'
import { useContext } from 'react'

const stores = {
  authStore
}

const isServer = typeof window === 'undefined'
useStaticRendering(isServer)

const newStores = {}

export function initializeStores (mobxStores = null) {
  Object.keys(stores).map(key => {
    if (isServer) {
      newStores[key] = new stores[key](
        isServer,
        mobxStores ? mobxStores[key] : null
      )
    } else if (typeof newStores[key] === 'undefined') {
      newStores[key] = new stores[key](
        isServer,
        mobxStores ? mobxStores[key] : null
      )
    }
  })
  return newStores
}

export const useStores = () => {
  return useContext(MobXProviderContext)
}
