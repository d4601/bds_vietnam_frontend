import { action, observable } from "mobx";

import Store from "./Store";
import { useObserver } from "mobx-react";
import { useStores } from "../stores";

import { saveAmount } from "../utils/auth";

class AuthStore extends Store {
  /**
   * Profile
   */
  @observable isAuthenticated = false;
  @observable me = null;
  @observable cartAmount = 0;

  /**
   * Profile
   */
  @action
  authSuccess = ({ profile }) => {
    console.log(profile);
    this.isAuthenticated = true;
    this.me = profile;
  };

  @action
  setAmountCart = (value) => {
    this.cartAmount = value;
  };

  @action
  addToCart = () => {
    this.cartAmount++;
    saveAmount(this.cartAmount);
  };

  @action
  logout = () => {
    this.isAuthenticated = false;
    this.me = null;
  };

  /* eslint-disable camelcase */
  @action
  updateProfile = ({ full_name, email, avatar, avatar_url }) => {
    this.me.full_name = full_name;
    this.me.email = email;
    this.me.avatar = avatar;
    this.me.avatar_url = avatar_url;
  };

  /**
   * Login / Register
   */
}

export default AuthStore;

export const useMeData = () => {
  const { authStore } = useStores();

  return useObserver(() => ({
    full_name: authStore.me?.fullName ?? "",
  }));
};
