import LoginPage, { getServerSideProps as getSSP } from '../../../features/auth/pages/login'

export const getServerSideProps = ctx => getSSP(ctx)

export default LoginPage
