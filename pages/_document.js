import Document, { Html, Head, Main, NextScript } from 'next/document'

import { ServerStyleSheet } from 'styled-components'
import { createEnvsFromList } from '../env'

class MyDocument extends Document {
  static getInitialProps (context) {
    const sheet = new ServerStyleSheet()
    const page = context.renderPage(App => props =>
      sheet.collectStyles(<App {...props} />)
    )

    const styleTags = sheet.getStyleElement()

    return {
      ...page,
      styleTags,
      language: context.language
    }
  }

  render () {
    const scriptEnv = `window.__ENV__ = ${JSON.stringify(
      createEnvsFromList()
    )};`

    return (
      <Html>
        <Head>
          <link
            rel='shortcut icon'
            type='image/png'
            href='/favicon.png'
          />
          <link
            rel='stylesheet'
            type='text/css'
            href='/static/fonts/fontawesome-pro/css/all.min.css'
          />

          <link
            rel='stylesheet'
            type='text/css'
            href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css'
          />

          <link
            rel='stylesheet' href='https://unpkg.com/leaflet@1.6.0/dist/leaflet.css'
            integrity='sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=='
            crossOrigin=''
          />
          <link
            rel='stylesheet'
            href='https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.css'
          />

          <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDdvWuu44VdR3vVyLgQRFNI-nKX3nTY43U&libraries=places' />
          <script src='https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.js' />
          <script src='https://open.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=IXeUegckPazCD4BEWR2CHZ494kZQdwBI' />
          <script src='https://open.mapquestapi.com/sdk/leaflet/v2.2/mq-routing.js?key=IXeUegckPazCD4BEWR2CHZ494kZQdwBI' />

          {this.props.styleTags}

          <script dangerouslySetInnerHTML={{ __html: scriptEnv }} />
        </Head>

        <body id='store-web-app'>
          <Main />
          <NextScript />
        </body>

      </Html>
    )
  }
}

export default MyDocument
