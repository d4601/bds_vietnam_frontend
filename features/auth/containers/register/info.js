import { inject, observer } from 'mobx-react'
import { routerLink, routerUtils } from 'router'
import { saveId, saveToken } from 'utils/authUtils'

import RegisterForm from 'features/auth/components/register-form'
import accountAPI from 'api/accountAPI'
import { useTranslation } from 'utils/i18nextUtils'

const RegisterInfoContainer = ({ authStore, saveLocationStore, serviceStore }) => {
  const { t: tGlobal } = useTranslation('global')

  const getErrorMessage = (errorRes) => {
    return errorRes?.error.map?.(e => tGlobal(`errorMessage.${e}`)).join?.(', ')
  }

  const handleSubmit = async (values) => {
    const { countryCode, phone, otpId, otpGroup, otpAuthCode } = authStore
    const { fullName, email, password, type } = values

    const registerBody = {
      full_name: fullName,
      email,
      password,
      type,

      country_code: countryCode,
      phone,
      otp_id: otpId,
      otp_group: otpGroup,
      otp_auth_code: otpAuthCode
    }
    console.log('POST /customeraccounts', registerBody)
    const registerRes = await accountAPI.customer.register(registerBody)
    console.log('Response : POST /customeraccounts', registerRes.data)

    if (!registerRes.success) {
      console.error({
        status: 'error',
        title: 'Đăng ký lỗi',
        description: getErrorMessage(registerRes.data),
        isClosable: true
      })
      return
    }

    const loginBody = {
      country_code: countryCode,
      login_account: phone,
      login_password: password
    }
    console.log('POST /customeraccounts/login', loginBody)
    const loginRes = await accountAPI.customer.login(loginBody)
    console.log('Response : POST /customeraccounts/login', loginRes.data)

    if (!loginRes.success) {
      console.error({
        status: 'error',
        title: 'Tự động đăng nhập lỗi',
        description: getErrorMessage(loginRes.data),
        isClosable: true
      })
      routerUtils.push(routerLink.auth.login.get().linkProps())
      return
    }

    const { id, token } = loginRes.data
    saveId(id)
    saveToken(token)
    authStore.authSuccess({ profile: loginRes.data })
    saveLocationStore.fetchAll()
    serviceStore.fetchAllService()

    routerUtils.push(routerLink.home.index.get().linkProps())
  }

  return (
    <RegisterForm
      onSubmit={handleSubmit}
    />
  )
}

export default inject('authStore', 'saveLocationStore', 'serviceStore')(observer(RegisterInfoContainer))
