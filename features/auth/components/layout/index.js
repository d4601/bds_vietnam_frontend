import { Container } from 'styled-bootstrap-grid'

import BoxShadow from '../../../../components/box-shadow'
import styled from 'styled-components'
import { Row, Col } from 'components/antd'
import NextHead from 'components/next-head'

const S4ChildrenWrapper = styled.div`
  min-height: 100vh;
  display: flex;
  align-items: center;
`

const S4Children = styled(BoxShadow)`
  margin: 16px 0;
  padding: 64px 32px;
  flex: 1;
`

const AuthLayout = ({ title, children }) => {
  return (
    <Container>
      <NextHead
        title={title}
      />

      <Row>
        <Col md={{ span: 12, offset: 6 }}>
          <S4ChildrenWrapper>
            <S4Children>
              {children}
            </S4Children>
          </S4ChildrenWrapper>
        </Col>
      </Row>
    </Container>
  )
}

export default AuthLayout
