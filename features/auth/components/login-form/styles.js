import styled from 'styled-components'

export const S4InputField = styled.div`
font-family: 'Roboto';
color: #333;
font-size: 1.2rem;
margin: 0 auto;
border-radius: 0.2rem;
background-color: rgb(255, 255, 255);
border: none;
width: 90%;
display: block;
border-bottom: 0.3rem solid transparent;
transition: all 0.3s;
`

export const S4Button = styled.div`

`
