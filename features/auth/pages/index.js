import AuthLayout from '../components/layout'
import AuthContainer from '../containers/login'

const AuthPage = () => {
  return (
    <AuthLayout title='Login'>
      <AuthContainer />
    </AuthLayout>
  )
}

AuthPage.getInitialProps = (ctx) => {
  return {
    namespacesRequired: ['global']
  }
}

export default AuthPage
