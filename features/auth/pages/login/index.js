import AuthLayout from '../../components/layout'
import LoginContainer from '../../containers/login'

const LoginPage = () => {
  return (
    <AuthLayout>
      <LoginContainer />
    </AuthLayout>
  )
}

export const getServerSideProps = async () => {
  return {
    props: {
      namespacesRequired: ['global']
    }
  }
}

export default LoginPage
