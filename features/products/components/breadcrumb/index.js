import React from 'react'
import { useRouter } from 'next/router'
import { PageHeader } from 'antd'

const MainPageHeader = ({
  title,
  subTitle = ''
}) => {
  const router = useRouter()
  const goBack = React.useCallback(() => {
    router.back()
  }, [])

  return (
    <PageHeader
      className='site-page-header boxed-width'
      onBack={goBack}
      title={title}
      subTitle={subTitle}
    />
  )
}

export default MainPageHeader
