import { ErrorMessage } from 'formik'
import FormErrorMessage from '../../components/form/form-error-message'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'
import themeConstants from 'theme/constants'

const S4FormControl = styled.div``

const S4FormLabel = styled.label`
  display: block;
  margin-bottom: 8px;
  font-size: ${themeConstants.fontSize.md};
  font-weight: ${themeConstants.fontWeights.medium};
`

const S4FormNote = styled.p`
  margin-top: 8px;
  font-size: ${themeConstants.fontSize.md};
  color: ${themeConstants.text.lightColor};
`

const createFormikField = (Component) => {
  const SelectField = ({
    field,
    form,

    label,
    note,

    ...componentProps
  }) => {
    const { name, value, onChange, onBlur } = field

    return (
      <S4FormControl>
        {label && <S4FormLabel>{label}</S4FormLabel>}

        <Component
          id={name}
          name={name}
          value={value}
          onChange={onChange}
          onBlur={onBlur}

          {...componentProps}
        />

        <ErrorMessage name={name} component={FormErrorMessage} />

        {note && <S4FormNote>{note}</S4FormNote>}
      </S4FormControl>
    )
  }

  SelectField.propTypes = {
    field: PropTypes.shape({
      name: PropTypes.any,
      value: PropTypes.any,
      onChange: PropTypes.func,
      onBlur: PropTypes.func
    }).isRequired,
    form: PropTypes.object.isRequired,

    label: PropTypes.string,
    note: PropTypes.string
  }

  return SelectField
}

export default createFormikField
