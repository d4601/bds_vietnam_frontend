const UrlPattern = require('url-pattern')
const queryString = require('query-string')

const REGEX_FAKE_PATH = /(\/:[a-zA-Z0-9]*)/g
const REGEX_REAL_PATH = /(=:[a-zA-Z0-9]*)/g
const REGEX_DOT_PATH = /(.:[a-zA-Z0-9]*)/g

const urlReplaceVariables = (slug, variables, prefix = '', defaultEmpty = '') => {
  let newSlug = slug
  Object.keys(variables).map(function (key) {
    if (variables[key]) {
      newSlug = newSlug.replace(prefix + ':' + key, prefix + variables[key])
    }
  })
  if (prefix === '/') {
    newSlug = newSlug.replace(REGEX_FAKE_PATH, prefix + defaultEmpty)
  } else if (prefix === '.') {
    newSlug = newSlug.replace(REGEX_DOT_PATH, prefix + defaultEmpty)
  } else if (prefix === '=') {
    newSlug = newSlug.replace(REGEX_REAL_PATH, prefix + defaultEmpty)
  }
  return newSlug
}

const replacePathExpress = (slug, variables) => {
  const urlPatternRegex = new UrlPattern(slug)
  const variablesKey = urlPatternRegex.names
  variablesKey.map(function (key) {
    if (
      typeof variables[key] === 'null' || // eslint-disable-line
      typeof variables[key] === 'undefined' // eslint-disable-line
    ) {
      variables[key] = 'null'
    }
  })
  return urlPatternRegex.stringify(variables).replace(/\?/g, '')
}

const routeInfo = (routeConfig, variables, isFullPath = false) => {
  const uAs = replacePathExpress(routeConfig.as, variables)
  const uHref = urlReplaceVariables(routeConfig.href, variables, '=', 'null')
  if (isFullPath) {
    return {
      href: uHref,
      url: uHref,
      as: uAs,
      file: routeConfig.file,
      linkProps: (query = null) => ({
        href: queryString.stringifyUrl({ url: uHref, query: query }),
        as: queryString.stringifyUrl({ url: uAs, query: query })
      })
    }
  } else {
    return uHref
  }
}

const createRouter = (router) => {
  const objectRouter = {}
  Object.keys(router).forEach(key => {
    objectRouter[key] = {}
    Object.keys(router[key]).forEach(childKey => {
      objectRouter[key][childKey] = {
        get: (variables = {}) => routeInfo(router[key][childKey], variables, true)
      }
    })
  })
  return objectRouter
}

module.exports = createRouter
