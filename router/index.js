const createRouter = require("./createRouter");
const routerUtils = require("./routerUtils");

const router = {
  home: {
    index: {
      href: "/",
      as: "/",
      file: "/",
    },
  },
  auth: {
    index: {
      href: "/auth",
      as: "/auth",
      file: "/auth",
    },
    login: {
      href: "/auth/login",
      as: "/auth/login",
      file: "/auth/login",
    },
    register: {
      href: "/auth/register",
      as: "/auth/register",
      file: "/auth/register",
    },
  },
  me: {
    all: {
      href: "/me/all",
      as: "/me/all",
      file: "/me/all",
    },
  },
};

module.exports.router = router;
module.exports.routerLink = createRouter(router);
module.exports.routerUtils = routerUtils;
