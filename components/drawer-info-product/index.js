import React, { useState } from "react";
import Link from "next/link";
import { routerLink } from "../../router";
import axios from "axios";

import {
  Drawer,
  Row,
  Col,
  Divider,
  Button,
  Typography,
  Rate,
  message,
} from "antd";
import { Steps } from "antd";
import { inject, observer } from "mobx-react";
import { useEffect } from "react";

const { Text, Title } = Typography;
const { Step } = Steps;

const DrawerProduct = ({
  onClose,
  visible,
  authStore,
  title,
  poster_path,
  overview,
  id,
  vote_average,
  vote_count,
  pubDate,
  link,
  disabled = false,
}) => {
  const success = () => {
    message.success("Book ticket success");
  };

  // useEffect(async () => {
  //   if (visible && isAuthenticated) {
  //     await axios.post("http://localhost:1337/referals", {
  //       link,
  //       refId,
  //       booking: me.username,
  //     });
  //   }
  // }, []);

  const refId = makeid(5);

  const handleAddToCart = async () => {
    onClose();
    await axios.post("http://localhost:1337/referals", {
      link,
      refId,
    });
    if (isAuthenticated) {
      authStore.addToCart();
      success();
    }
  };

  const { isAuthenticated, me } = authStore;

  function makeid(length) {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  return (
    <>
      {!isAuthenticated ? (
        <Drawer
          title="Thông tin bài viết"
          width={880}
          placement="right"
          closable={false}
          onClose={onClose}
          visible={visible}
          bodyStyle={{ paddingBottom: 80 }}
          footer={
            !isAuthenticated ? (
              <a href={`${link}?ref=${refId}`} target="_blank">
                <Button
                  htmlType="submit"
                  type="primary"
                  block
                  onClick={handleAddToCart}
                  disabled={disabled}
                >
                  Xem
                </Button>
              </a>
            ) : (
              <Button
                htmlType="submit"
                type="primary"
                block
                onClick={handleAddToCart}
                disabled={disabled}
              >
                Xem
              </Button>
            )
          }
        >
          <Title level={2} style={{ marginBottom: 24 }}>
            {title}.
          </Title>
          <Row>
            <Col span={12}>
              <img
                alt={title}
                src={`${poster_path}`}
                style={{
                  objectFit: "contain",
                  width: "95%",
                  height: "auto",
                  borderRadius: "8px",
                }}
              />
            </Col>
            <Col span={12}>
              <Title type="danger" strong level={3}>
                {pubDate}
              </Title>

              {/* <Col span={12}> */}
              <Title level={5}>{overview}</Title>
              {/* </Col> */}
            </Col>
          </Row>

          <Divider />

          <Row>
            <Col span={12}>
              <Rate disabled defaultValue={5} />
            </Col>
            <Col span={12}></Col>
          </Row>

          <Row style={{ marginTop: "88px", marginLeft: "88px" }}>
            <Col span={6}>
              <img
                alt="example"
                src="/images/carousel/Group-2.svg"
                style={{
                  objectFit: "contain",
                  width: "95%",
                  height: "auto",
                  borderRadius: "8px",
                }}
              />
            </Col>
            <Col span={12} style={{ marginTop: "16px" }}>
              <Title type="sucess" strong level={3}>
                Hãy để lại thông tin cho chúng tôi
              </Title>
              <Typography>Chuyên gia của chúng tôi sẽ liên hệ lại</Typography>
              <Divider />
              <Step
                status="wait"
                title={
                  <Link {...routerLink.auth.login.get().linkProps()}>
                    Để lại thông tin tư vấn
                  </Link>
                }
              />
            </Col>
          </Row>
        </Drawer>
      ) : (
        <Drawer
          title="Bài viết"
          width={1100}
          // placement={placement}
          closable
          onClose={onClose}
          visible={visible}
          style={{ paddingBottom: "0px" }}
          bodyStyle={{
            height: "calc(100vh)",
          }}
        >
          <iframe src={link} width="100%" height="100%" border="8px" />
        </Drawer>
      )}
    </>
  );
};

export default inject("authStore")(observer(DrawerProduct));
