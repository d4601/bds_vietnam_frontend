import React from 'react'
import PropTypes from 'prop-types'
import { Switch as AntdSwitch } from 'components/antd'

const Switch = ({
  name,
  value,
  onChange
}) => {
  const handleChange = (checked) => {
    const event = {
      target: {
        name,
        value: checked
      }
    }

    onChange && onChange(event)
  }

  return (
    <AntdSwitch
      id={name}
      checked={value}
      onChange={handleChange}
    />
  )
}

Switch.propTypes = {
  name: PropTypes.string,
  value: PropTypes.any,
  onChange: PropTypes.func
}

export default Switch
