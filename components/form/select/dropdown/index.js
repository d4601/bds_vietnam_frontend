import PropTypes from 'prop-types'
import { Select } from 'components/antd'
import _ from 'lodash'
import slug from 'slug'

const getDefaultValue = value => {
  if (!value) return undefined

  if (_.isArray(value)) {
    // Array
    return value.map(item => _.isString(item) || _.isNumber(item)
      ? item
      : item?.value)
  }

  if (_.isObject(value)) {
    // Object
    return value?.value
  }

  // String
  return value
}

const Dropdown = ({
  options = [],
  name,
  value,

  onChange,
  onBlur,

  isInvalid,
  ...props
}) => {
  const handleChange = (value) => {
    const event = {
      target: {
        name,
        value
      }
    }

    onChange && onChange(event)
  }

  const handleBlur = () => {
    const event = {
      target: {
        name
      }
    }

    onBlur && onBlur(event)
  }

  return (
    <Select
      defaultValue={getDefaultValue(value)}
      allowClear

      showSearch
      optionFilterProp='children'
      filterOption={(input, option) => {
        const optionValue = slug(option.children.toLowerCase())
        const inputValue = slug(input.toLowerCase())
        return optionValue.indexOf(inputValue) >= 0
      }}

      style={{ width: '100%' }}
      onChange={handleChange}
      onBlur={handleBlur}

      {...props}
    >
      {options.map(option => (
        <Select.Option
          key={option.value}
          value={option.value}
        >
          {option.label}
        </Select.Option>
      ))}
    </Select>
  )
}

const OptionValueType = PropTypes.oneOfType([PropTypes.string, PropTypes.number])

Dropdown.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: OptionValueType
  })),

  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,

    // { value: "vi" }
    // or { value: 1 }
    PropTypes.shape({
      value: OptionValueType
    }),

    // ["vi", "en"]
    // or [0, 1]
    PropTypes.arrayOf(OptionValueType),

    // [{ value: "vi" }, { value: "en" }]
    // or [{ value: 0 }, { value: 1 }]
    PropTypes.arrayOf(PropTypes.shape({
      value: OptionValueType
    }))
  ])
}

export default Dropdown
