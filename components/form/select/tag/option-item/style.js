import styled from 'styled-components'
import themeConstants from 'theme/constants'

export const S4OptionItem = styled.a`
  display: flex;
  align-items: center;
  min-height: 40px;

  border: 1px solid ${themeConstants.border.defaultColor};
  border-radius: 4px;

  padding-left: 16px;
  padding-right: 16px;
  padding-top: 8px;
  padding-bottom: 8px;

  background: #fff;
  transition: ${themeConstants.transition['0.24']};
  cursor: pointer;

  .fa-check-circle {
    margin-left: 16px;
  }

  &.selected {
    background: #F9F6FA;
    border-color: ${themeConstants.color.primary[500]};
    color: ${themeConstants.color.primary[500]} !important;
  }

  &.disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }

  &:not(.horizontal) {
    &:not(:last-child) {
      margin-bottom: 8px;
    }
  }

  &.horizontal {
    margin-right: 8px;
    margin-bottom: 8px;

    .fa-check-circle {
      display: none;
    }

    &.rounded-pill {
      padding-left: 24px;
      padding-right: 24px;

      .icon {
        margin-right: 12px;
      }
    }
  }
`

export const S4OptionIcon = styled.div`
  margin-right: 16px;
  width: 32px;
  height: 32px;

  display: flex;
  align-items: center;
  justify-content: center;

  img {
    width: 32px;
    height: 32px;
  }
`

export const S4OptionInfo = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;

  .label {
    color: #000;
    font-weight: ${themeConstants.fontWeights.medium};
  }

  .description {
    margin-top: 1px;
    color: ${themeConstants.text.lightColor};
    font-size: ${themeConstants.fontSize.sm};
  }
`
