import { S4OptionIcon, S4OptionInfo, S4OptionItem } from './style'

import PropTypes from 'prop-types'
import classNames from 'classnames'

const OptionItem = ({
  direction,
  isRounded,

  icon,
  label,
  description,
  value,

  isSelected,
  disabled,
  onClick
}) => {
  return (
    <S4OptionItem
      key={value}
      className={classNames({
        selected: isSelected,
        disabled: disabled,
        [direction]: true,
        'rounded-pill': direction === 'horizontal' && isRounded
      })}
      onClick={() => !disabled && onClick(value)}
    >
      {icon && (
        <S4OptionIcon className='icon'>
          {typeof icon === 'string'
            ? <img src={icon} />
            : icon}
        </S4OptionIcon>
      )}

      <S4OptionInfo className='long-and-truncated'>
        <span className='label'>{label}</span>
        {description && <span className='description'>{description}</span>}
      </S4OptionInfo>

      {isSelected && <i className='fas fa-check-circle' />}
    </S4OptionItem>
  )
}

const OptionValueType = PropTypes.oneOfType([PropTypes.string, PropTypes.number])

OptionItem.propTypes = {
  direction: PropTypes.oneOf(['horizontal', 'vertical']),
  isRounded: PropTypes.bool,

  icon: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string
  ]),
  label: PropTypes.string,
  value: OptionValueType,
  description: PropTypes.string,

  isSelected: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func
}

export default OptionItem
