import OptionItem from './option-item'
import PropTypes from 'prop-types'
import { S4OptionList } from './style'
import classNames from 'classnames'
import useSelectTag from 'hooks/use-select-tag'

const SelectTag = ({
  direction = 'horizontal',
  isRounded = false,

  mode = 'single',
  options = [],

  name,
  value,
  onChange,

  ...props
}) => {
  const {
    checkSelected,
    onOptionClick
  } = useSelectTag({
    mode,
    name,
    value,
    onChange
  })

  return (
    <S4OptionList className={classNames(direction)}>
      {options.map(option => {
        const isSelected = checkSelected(option.value)

        return (
          <OptionItem
            key={option.value}
            direction={direction}
            isRounded={isRounded}

            icon={option.icon}
            label={option.label}
            description={option.description}
            value={option.value}

            isSelected={isSelected}
            disabled={option.disabled}
            onClick={onOptionClick}
          />
        )
      })}
    </S4OptionList>
  )
}

const OptionValueType = PropTypes.oneOfType([PropTypes.string, PropTypes.number])

const ValueType = PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.number,

  // { value: "vi" }
  PropTypes.shape({
    value: OptionValueType
  }),

  // ["vi", "en"]
  // or [0, 1]
  PropTypes.arrayOf(OptionValueType),

  // [{ value: "vi" }, { value: "en" }]
  // or [{ value: 0 }, { value: 1 }]
  PropTypes.arrayOf(PropTypes.shape({
    value: OptionValueType
  }))
])

SelectTag.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    icon: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.string
    ]),

    label: PropTypes.string,
    value: OptionValueType,

    description: PropTypes.string
  })),

  defaultValue: ValueType,
  value: ValueType,

  onChange: PropTypes.func,
  mode: PropTypes.oneOf(['single', 'multiple']),
  direction: PropTypes.oneOf(['horizontal', 'vertical']),
  isRounded: PropTypes.bool
}

export default SelectTag
