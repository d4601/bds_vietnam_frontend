import PropTypes from 'prop-types'
import ReactOTPInput from 'lib/react-otp-input'
import Clearfix from 'components/clearfix'

/**
 * Render <OTPInput /> component
 * @param {Function} props.onChange - (event) => { console.log(event.target.value) }
 */
const OTPInput = ({
  name,
  onChange,
  onBlur,
  value,
  isInvalid,

  numInputs = 6,
  isInputNum = true,
  secureTextEntry = false,
  isDisabled = false,
  autoFocus = false
}) => {
  const handleChange = newOTP => {
    onChange && onChange({
      target: {
        name,
        value: newOTP
      }
    })
  }

  const handleBlur = () => {
    onBlur && onBlur({
      target: {
        name
      }
    })
  }

  return (
    <ReactOTPInput
      name={name}
      numInputs={numInputs}
      value={value}
      onChange={handleChange}
      onBlur={handleBlur}

      separator={<Clearfix width={8} height={8} />}
      inputStyle='otp-input'
      errorStyle='otp-input-error'
      containerStyle='otp-container'

      isDisabled={isDisabled}
      isInputNum={isInputNum}
      hasErrored={isInvalid}
      secureTextEntry={secureTextEntry}

      shouldAutoFocus={autoFocus}
    />
  )
}

OTPInput.propTypes = {
  name: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  isInvalid: PropTypes.bool,

  numInputs: PropTypes.number,
  isInputNum: PropTypes.bool,
  secureTextEntry: PropTypes.bool,
  isDisabled: PropTypes.bool,
  autoFocus: PropTypes.bool
}

export default OTPInput
