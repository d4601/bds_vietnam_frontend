import { InputNumber } from 'components/antd'
import PropTypes from 'prop-types'
import React from 'react'
import classNames from 'classnames'
import styled from 'styled-components'
import themeConstants from 'theme/constants'

const S4InputNumber = styled.div`
  position: relative;

  .ant-input-number {
    width: 100%;

    &-input {
      padding-right: 32px;
    }

    &-handler-wrap {
      display: none;
      z-index: 1;
    }
  }

  &.with-icon {
    .ant-input-number {
      &-input {
        padding-left: ${16 + 14 + 15}px;
      }
    }
  }
`

export const S4Prefix = styled.div`
  position: absolute;
  left: 16px;
  top: 4px;
  z-index: 1;

  display: flex;
  align-items: center;
  justify-content: center;

  width: 14px;
  height: 32px;
`

const S4Suffix = styled.div`
  position: absolute;
  right: 16px;
  top: 4px;
  z-index: 0;

  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;

  span {
    font-weight: ${themeConstants.fontWeights.medium};
  }
`

const MoneyInput = ({
  className,
  style,

  id,
  name,
  value,

  onChange,
  onBlur,

  icon,
  placeholder,
  disabled
}) => {
  const handleChange = (value) => {
    onChange && onChange({
      target: {
        name,
        value
      }
    })
  }

  const handleBlur = () => {
    onBlur && onBlur({
      target: {
        name
      }
    })
  }

  return (
    <S4InputNumber className={classNames(className, { 'with-icon': icon })} style={style}>
      {icon && (
        <S4Prefix />
      )}

      <InputNumber
        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
        parser={value => value.replace(/(\.*)/g, '')}

        id={id}
        name={name}

        value={value}
        onChange={handleChange}
        onBlur={handleBlur}

        placeholder={placeholder || 'Nhập số tiền'}
        disabled={disabled}
      />

      <S4Suffix>
        <span>đ</span>
      </S4Suffix>
    </S4InputNumber>
  )
}

MoneyInput.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,

  id: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  onChange: PropTypes.func,
  onBlur: PropTypes.func,

  icon: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool
}

export default MoneyInput
