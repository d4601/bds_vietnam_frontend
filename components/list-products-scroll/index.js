import React, { useState } from "react";
import { List, Space, Spin } from "antd";
import { ShoppingOutlined, StarOutlined } from "@ant-design/icons";

import DrawerProduct from "../drawer-info-product";

import InfiniteScroll from "react-infinite-scroller";

const IconText = ({ icon, text }) => (
  <Space>
    {React.createElement(icon)}
    {text}
  </Space>
);

const data = [
  {
    title: {
      __cdata: "Khởi động loạt công trình hạ tầng 'khủng' giúp tăng nhiệt BĐS",
    },
    link:
      "https://toancanhbatdongsan.com.vn/khoi-dong-cong-trinh-ha-tang-tang-nhiet-bds-a798",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/khoi-dong-cong-trinh-ha-tang-tang-nhiet-bds-a798",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/khoi-dong-cong-trinh-ha-tang-tang-nhiet-bds-a798"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/08/cau-van-tien-quang-ninh-1638927340.jpg" align="left" hspace="5" /></a>Cuối năm 2021 và đầu năm 2022, hàng loạt công trình hạ tầng "khủng" được triển khai, giao thông kết nối liên vùng Bắc – Nam được hoàn thiện sẽ tạo đà cho thị trường bất động sản khu vực lân cận bứt phá trong thời gian tới.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Nguyễn Tuân",
    },
    pubDate: "2021-12-08 08:59:31",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/08/cau-van-tien-quang-ninh-1638927340.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Hiểu đúng về smart city",
    },
    link: "https://toancanhbatdongsan.com.vn/hieu-dung-ve-smart-city-a735",
    guid: {
      _isPermaLink: "true",
      __text: "https://toancanhbatdongsan.com.vn/hieu-dung-ve-smart-city-a735",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/hieu-dung-ve-smart-city-a735"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/11/12/dai-bac-dai-loan-smart-city-min-1636678445.jpg" align="left" hspace="5" /></a>Quá trình đô thị hóa diễn ra mạnh mẽ đã khiến những nhà cầm quyền đối mặt với những vấn đề xã hội lớn như kẹt xe, ô nhiễm môi trường, tài nguyên khan hiếm,... Chính vì thế Smart City đã trở thành giải pháp chiến lược để giải quyết các tình trạng trên.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Thế Nghị",
    },
    pubDate: "2021-11-12 08:12:43",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/11/12/dai-bac-dai-loan-smart-city-min-1636678445.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Top 5 quy hoạch đô thị độc đáo trên thế giới",
    },
    link:
      "https://toancanhbatdongsan.com.vn/top-5-quy-hoach-do-thi-the-gioi-a627",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/top-5-quy-hoach-do-thi-the-gioi-a627",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/top-5-quy-hoach-do-thi-the-gioi-a627"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/10/02/do-thi-naerum-1633183431.jpg" align="left" hspace="5" /></a>Các đô thị này có quy hoạch và thiết kế không chỉ chú trọng thẩm mỹ mà còn mang tầm nhìn xa trong các giải pháp về giao thông, môi trường',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Hồng Ân",
    },
    pubDate: "2021-10-03 10:05:17",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/10/02/do-thi-naerum-1633183431.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "60% doanh nghiệp bất động sản chưa đầu tư R&amp;D",
    },
    link:
      "https://toancanhbatdongsan.com.vn/60-doanh-nghiep-bat-dong-san-chua-co-phong-rd-a596",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/60-doanh-nghiep-bat-dong-san-chua-co-phong-rd-a596",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/60-doanh-nghiep-bat-dong-san-chua-co-phong-rd-a596"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/09/22/tong-the-du-an-the-manor-central-park-1-1632300206.jpg" align="left" hspace="5" /></a>Khảo sát sơ bộ trên 50 đại diện doanh nghiệp của RealCom chỉ ra, hơn 60% đại diện doanh nghiệp hiểu rằng R&D quan trọng, nhưng chưa có bộ phận chuyên trách.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Toàn Anh",
    },
    pubDate: "2021-09-22 18:00:00",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/09/22/tong-the-du-an-the-manor-central-park-1-1632300206.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "TP.HCM kiến nghị bổ sung khu công nghiệp 668ha tại Bình Chánh",
    },
    link:
      "https://toancanhbatdongsan.com.vn/tphcm-kien-nghi-bo-sung-khu-cong-nghiep-668ha-tai-binh-chanh-a312",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/tphcm-kien-nghi-bo-sung-khu-cong-nghiep-668ha-tai-binh-chanh-a312",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/tphcm-kien-nghi-bo-sung-khu-cong-nghiep-668ha-tai-binh-chanh-a312"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/30/khu-cong-nghiep-freepik-1625036993.jpg" align="left" hspace="5" /></a>UBND TP.HCM vừa trình Chính phủ bổ sung khu công nghiệp Phạm Văn Hai có diện tích 668ha vào quy hoạch phát triển các khu công nghiệp trên địa bàn, để thay thế 3 khu công nghiệp chậm triển khai là Bàu Đưng, Phước Hiệp và Xuân Thới Thượng.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Ngân Khánh",
    },
    pubDate: "2021-06-30 14:16:59",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/30/khu-cong-nghiep-freepik-1625036993.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Lâm Đồng sẽ là động lực kinh tế của Tây Nguyên",
    },
    link:
      "https://toancanhbatdongsan.com.vn/lam-dong-se-la-dong-luc-kinh-te-cua-tay-nguyen-a271",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/lam-dong-se-la-dong-luc-kinh-te-cua-tay-nguyen-a271",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/lam-dong-se-la-dong-luc-kinh-te-cua-tay-nguyen-a271"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/19/lam-dong-1624121861.jpg" align="left" hspace="5" /></a>Lâm Đồng được định hướng trở thành một khu vực kinh tế động lực của vùng Tây Nguyên, phát triển kinh tế - xã hội đảm bảo môi trường và khuyến khích "tăng trưởng xanh".',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Chiêu An",
    },
    pubDate: "2021-06-20 07:01:24",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/19/lam-dong-1624121861.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "9.005 tỷ đồng để xây dựng sân bay Gò Găng",
    },
    link:
      "https://toancanhbatdongsan.com.vn/9005-ty-dong-de-xay-dung-san-bay-go-gang-a263",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/9005-ty-dong-de-xay-dung-san-bay-go-gang-a263",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/9005-ty-dong-de-xay-dung-san-bay-go-gang-a263"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/18/san-bay-envato-1624014175.jpg" align="left" hspace="5" /></a>Dự án sân bay Gò Găng dự kiến được xây dựng trên đảo Gò Găng, xã Long Sơn, TP. Vũng Tàu, với tổng mức đầu tư xây dựng hoàn chỉnh hơn 9.005 tỷ đồng.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Ngân Khánh",
    },
    pubDate: "2021-06-18 18:05:17",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/18/san-bay-envato-1624014175.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Tuyến Metro số 2 sẽ hoàn tất giải phóng mặt bằng trong năm nay",
    },
    link:
      "https://toancanhbatdongsan.com.vn/tuyen-metro-so-2-se-hoan-tat-giai-phong-mat-bang-trong-nam-nay-a262",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/tuyen-metro-so-2-se-hoan-tat-giai-phong-mat-bang-trong-nam-nay-a262",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/tuyen-metro-so-2-se-hoan-tat-giai-phong-mat-bang-trong-nam-nay-a262"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/18/metro-so-2-vnexpress-1623993488.jpg" align="left" hspace="5" /></a>Hơn 251.000m2 diện tích đất phục vụ cho việc thi công tuyến Metro số 2 (Bến Thành – Tham Lương) sẽ được bàn giao cho các nhà thầu trong 6 tháng tới.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Ngân Khánh",
    },
    pubDate: "2021-06-18 12:19:07",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/18/metro-so-2-vnexpress-1623993488.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Quy hoạch khu công nghệ thông tin tập trung hơn 20ha tại Cần Thơ",
    },
    link:
      "https://toancanhbatdongsan.com.vn/quy-hoach-khu-cong-nghe-thong-tin-tap-trung-hon-20ha-tai-can-tho-a224",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/quy-hoach-khu-cong-nghe-thong-tin-tap-trung-hon-20ha-tai-can-tho-a224",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/quy-hoach-khu-cong-nghe-thong-tin-tap-trung-hon-20ha-tai-can-tho-a224"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/10/khu-cong-nghe-quang-trung-1623301183.jpg" align="left" hspace="5" /></a>Phó Thủ tướng Vũ Đức Đam vừa ký quyết định thành lập khu công nghệ thông tin tập trung thứ 8 của Việt Nam, với quy mô hơn 20ha.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Ngân Khánh",
    },
    pubDate: "2021-06-10 19:01:24",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/10/khu-cong-nghe-quang-trung-1623301183.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Quảng Ngãi sẽ cấp chủ trương đầu tư cho 'siêu dự án' gang thép 85.000 tỷ đồng",
    },
    link:
      "https://toancanhbatdongsan.com.vn/quang-ngai-se-cap-chu-truong-dau-tu-cho-sieu-du-an-gang-thep-a221",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/quang-ngai-se-cap-chu-truong-dau-tu-cho-sieu-du-an-gang-thep-a221",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/quang-ngai-se-cap-chu-truong-dau-tu-cho-sieu-du-an-gang-thep-a221"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/10/nha-may-thep-1-1623297679.jpg" align="left" hspace="5" /></a>Với tổng vốn đầu tư khoảng 85.000 tỷ đồng, dự án khu liên hợp sản xuất gang thép Hòa Phát - Dung Quất 2 dự kiến sẽ giải quyết việc làm cho hơn 7.500 lao động với công suất dự kiến 5,6 triệu tấn/năm.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Ngân Khánh",
    },
    pubDate: "2021-06-10 18:02:38",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/10/nha-may-thep-1-1623297679.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Hà Nội xây dựng Chương trình phát triển nhà ở giai đoạn 2021 - 2030",
    },
    link:
      "https://toancanhbatdongsan.com.vn/ha-noi-xay-dung-chuong-trinh-phat-trien-nha-o-giai-doan-2021-2030-a219",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/ha-noi-xay-dung-chuong-trinh-phat-trien-nha-o-giai-doan-2021-2030-a219",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/ha-noi-xay-dung-chuong-trinh-phat-trien-nha-o-giai-doan-2021-2030-a219"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/09/toan-canh-ha-noi-1623230007.jpg" align="left" hspace="5" /></a>Chương trình phát triển nhà ở TP Hà Nội giai đoạn 2021-2030 và định hướng đến năm 2040 nhằm đáp ứng cơ bản nhu cầu nhà ở của người dân, đồng thời gắn với phát triển đô thị theo hướng xanh, thông minh, bền vững, hiện đại.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Ngân Khánh",
    },
    pubDate: "2021-06-09 22:36:09",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/09/toan-canh-ha-noi-1623230007.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Hà Nội sẽ chi hơn 330.000 tỷ đồng cho hạ tầng giao thông",
    },
    link:
      "https://toancanhbatdongsan.com.vn/ha-noi-se-chi-hon-330000-ty-dong-cho-ha-tang-giao-thong-a197",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/ha-noi-se-chi-hon-330000-ty-dong-cho-ha-tang-giao-thong-a197",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/ha-noi-se-chi-hon-330000-ty-dong-cho-ha-tang-giao-thong-a197"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/05/giao-thong-ha-noi-1622871754.jpg" align="left" hspace="5" /></a>Sở Giao thông Vận tải Hà Nội dự kiến có 460 dự án phát triển hạ tầng giao thông vận tải xây dựng trong 5 năm tới tại thành phố, với tổng kinh phí hơn 330.000 tỷ đồng.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Chiêu An",
    },
    pubDate: "2021-06-05 19:46:05",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/05/giao-thong-ha-noi-1622871754.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Quảng Ninh duyệt quy hoạch tổ hợp vui chơi, giải trí cao cấp quy mô hơn 4.300ha",
    },
    link:
      "https://toancanhbatdongsan.com.vn/quang-ninh-duyet-quy-hoach-to-hop-vui-choi-giai-tri-cao-cap-quy-mo-hon-4300ha-a195",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/quang-ninh-duyet-quy-hoach-to-hop-vui-choi-giai-tri-cao-cap-quy-mo-hon-4300ha-a195",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/quang-ninh-duyet-quy-hoach-to-hop-vui-choi-giai-tri-cao-cap-quy-mo-hon-4300ha-a195"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/05/khu-kinh-te-van-don-1622856506.jpeg" align="left" hspace="5" /></a>UBND tỉnh Quảng Ninh vừa phê duyệt quy hoạch phân khu xây dựng tỷ lệ 1/2.000  khu vực Đông Bắc Cái Bầu, khu kinh tế Vân Đồn. Nơi đây sẽ trở thành khu du lịch nghỉ dưỡng cao cấp và khu giải trí có casino, sân golf…trong tương lai',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Ngân Khánh",
    },
    pubDate: "2021-06-05 15:29:35",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/05/khu-kinh-te-van-don-1622856506.jpeg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "TP.HCM chi 1.500 tỷ đồng mở rộng Quốc lộ 50",
    },
    link:
      "https://toancanhbatdongsan.com.vn/tphcm-chi-1500-ty-dong-mo-rong-quoc-lo-50-a140",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/tphcm-chi-1500-ty-dong-mo-rong-quoc-lo-50-a140",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/tphcm-chi-1500-ty-dong-mo-rong-quoc-lo-50-a140"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/05/27/quoc-lo-50-1622113063.jpg" align="left" hspace="5" /></a>Quốc lộ 50, đoạn qua huyện Bình Chánh, sẽ được mở rộng lên 34m nhằm góp phần giảm tai nạn, ùn tắc và tăng kết nối liên vùng.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "T.H",
    },
    pubDate: "2021-05-28 00:16:22",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/05/27/quoc-lo-50-1622113063.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Bà Rịa - Vũng Tàu đấu thầu chọn nhà đầu tư khu đô thị Gò Găng",
    },
    link:
      "https://toancanhbatdongsan.com.vn/ba-ria-vung-tau-dau-thau-chon-nha-dau-tu-du-an-khu-do-thi-go-gang-a124",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/ba-ria-vung-tau-dau-thau-chon-nha-dau-tu-du-an-khu-do-thi-go-gang-a124",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/ba-ria-vung-tau-dau-thau-chon-nha-dau-tu-du-an-khu-do-thi-go-gang-a124"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/25/dao-go-gang-dulichvietnam-1621922623.jpg" align="left" hspace="5" /></a>UBND tỉnh Bà Rịa - Vũng Tàu vừa chỉ đạo tổ chức đấu thầu để chọn nhà đầu tư cho dự án khu đô thị Gò Găng (đảo Gò Găng, thành phố Vũng Tàu).',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "T.H",
    },
    pubDate: "2021-05-22 20:05:27",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/25/dao-go-gang-dulichvietnam-1621922623.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Cao tốc TP.HCM - Mộc Bài lại đội vốn thêm gần 2.300 tỷ đồng",
    },
    link:
      "https://toancanhbatdongsan.com.vn/cao-toc-tphcm-moc-bai-lai-doi-von-them-gan-2300-ty-dong-a123",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/cao-toc-tphcm-moc-bai-lai-doi-von-them-gan-2300-ty-dong-a123",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/cao-toc-tphcm-moc-bai-lai-doi-von-them-gan-2300-ty-dong-a123"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/25/so-do-cao-toc-tphcm-moc-bai-tong-cuc-duong-bo-1621919843.jpg" align="left" hspace="5" /></a>Sở Giao thông Vận tải TP.HCM vừa điều chỉnh tổng mức đầu tư cao tốc TP.HCM - Mộc Bài lên 15.900 tỷ đồng, tăng 2.286 tỷ đồng so với đề xuất trước đó.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "T.H",
    },
    pubDate: "2021-05-22 19:18:24",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/25/so-do-cao-toc-tphcm-moc-bai-tong-cuc-duong-bo-1621919843.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Đắk Nông kêu gọi đầu tư vào khu công nghiệp hơn 4.200 tỷ đồng",
    },
    link:
      "https://toancanhbatdongsan.com.vn/dak-nong-keu-goi-dau-tu-vao-khu-cong-nghiep-hon-4200-ty-dong-a122",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/dak-nong-keu-goi-dau-tu-vao-khu-cong-nghiep-hon-4200-ty-dong-a122",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/dak-nong-keu-goi-dau-tu-vao-khu-cong-nghiep-hon-4200-ty-dong-a122"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/25/khu-cong-nghiep-nhan-co-2-bqlkcn-dak-nong-1621918470.jpg" align="left" hspace="5" /></a>UBND tỉnh Đắk Nông vừa công bố thông tin, kêu gọi đầu tư vào dự án khu công nghiệp Nhân Cơ 2, có tổng vốn đầu tư hơn 4.200 tỷ đồng.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "T.H",
    },
    pubDate: "2021-05-21 19:00:57",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/25/khu-cong-nghiep-nhan-co-2-bqlkcn-dak-nong-1621918470.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Sắp có cao tốc, Đồng Tháp đi TP.HCM chỉ còn 2 tiếng",
    },
    link:
      "https://toancanhbatdongsan.com.vn/sap-co-cao-toc-dong-thap-di-tphcm-chi-con-2-tieng-a121",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/sap-co-cao-toc-dong-thap-di-tphcm-chi-con-2-tieng-a121",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/sap-co-cao-toc-dong-thap-di-tphcm-chi-con-2-tieng-a121"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/25/so-do-19-cao-toc-phia-nam-ban-quan-ly-du-an-so7-1621917420.png" align="left" hspace="5" /></a>Hai tuyến cao tốc Mỹ An - Cao Lãnh và An Hữu - Cao Lãnh chuẩn bị đầu tư sẽ giúp rút thời gian di chuyển từ Đồng Tháp đến TP.HCM còn 2 tiếng thay vì 4 tiếng như hiện nay.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "T.H",
    },
    pubDate: "2021-05-19 18:38:28",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/25/so-do-19-cao-toc-phia-nam-ban-quan-ly-du-an-so7-1621917420.png",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Nha Trang: Phát triển cao tầng nhiều khu vực để giảm mật độ xây dựng",
    },
    link:
      "https://toancanhbatdongsan.com.vn/nha-trang-phat-trien-cao-tang-nhieu-khu-vuc-de-giam-mat-do-xay-dung-a166",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/nha-trang-phat-trien-cao-tang-nhieu-khu-vuc-de-giam-mat-do-xay-dung-a166",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/nha-trang-phat-trien-cao-tang-nhieu-khu-vuc-de-giam-mat-do-xay-dung-a166"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/05/31/bien-nha-trang-ngan-khanh-1622454974.jpg" align="left" hspace="5" /></a>Đây là một trong những đề xuất được ghi nhận tại hội thảo lắng nghe ý kiến chuyên gia về việc điều chỉnh quy hoạch chung TP. Nha Trang đến năm 2040 mới đây.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Ngân Khánh",
    },
    pubDate: "2021-05-10 23:59:38",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/05/31/bien-nha-trang-ngan-khanh-1622454974.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
];

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

const ListProducts = ({ loading, fetchMoreItem, hasMore }) => {
  const [visible, setVisible] = useState(false);
  const [info, setInfo] = useState();

  const onClose = () => {
    setVisible(false);
  };

  return (
    <>
      <InfiniteScroll
        initialLoad={false}
        loadMore={fetchMoreItem}
        hasMore={hasMore}
        loader={
          <Spin
            style={{
              display: "block",
              marginLeft: "auto",
              marginRight: "auto",
            }}
          />
        }
      >
        <List
          itemLayout="vertical"
          size="small"
          dataSource={data}
          style={{ margin: "48px" }}
          renderItem={(item) => (
            <List.Item
              key={item.title.__cdata}
              extra={
                <img
                  width={272}
                  style={{ borderRadius: "10px" }}
                  alt="logo"
                  src={item.content._url}
                />
              }
            >
              <List.Item.Meta
                title={<a href={item.href}>{item.title.__cdata}</a>}
                description={item.description.__cdata.split("</a>")[1]}
                onClick={() => {
                  setVisible(true);
                  setInfo(item);
                }}
              />
              {item.pubDate} <br /> <br />
              <br />
              <br />
              <br />
              <a target="_blank" href={`${item.link}?ref=${makeid(5)}`}>
                Xem thêm
              </a>
            </List.Item>
          )}
        />
      </InfiniteScroll>
      <DrawerProduct
        visible={visible}
        onClose={onClose}
        title={info?.title?.__cdata}
        overview={info?.description?.__cdata.split("</a>")[1]}
        poster_path={info?.content?._url}
        id={info?.id}
        pubDate={info?.pubDate}
        link={info?.link}
      />
    </>
  );
};

export default ListProducts;
