import React from "react";
import { List, Card } from "antd";
import { useEffect, useState } from "react";
import DrawerProduct from "../../components/drawer-info-product";

const { Meta } = Card;

const gridStyle = {
  textAlign: "center",
};

const data = [
  {
    title: {
      __cdata: "Hàng loạt dự án trọng điểm triển khai trong năm 2022",
    },
    link:
      "https://toancanhbatdongsan.com.vn/du-an-trong-diem-trien-khai-nam-2022-a855",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/du-an-trong-diem-trien-khai-nam-2022-a855",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/du-an-trong-diem-trien-khai-nam-2022-a855"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/30/toancanhbatdongsan-thumbnail-4-1640822385.jpg" align="left" hspace="5" /></a>Trong năm 2022 hàng loạt công trình cao tốc Dầu Giây – Liên Khương, nút giao An Phú, nhà ga sân bay Long Thành,… ấn định thời điểm khởi công',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Hồng Ân",
    },
    pubDate: "2021-12-30 07:30:29",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/30/toancanhbatdongsan-thumbnail-4-1640822385.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Khởi công dự án phức hợp 50 tầng cao nhất Hà Đông",
    },
    link:
      "https://toancanhbatdongsan.com.vn/khoi-cong-du-an-phuc-hop-50-tang-cao-nhat-ha-dong-a760",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/khoi-cong-du-an-phuc-hop-50-tang-cao-nhat-ha-dong-a760",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/khoi-cong-du-an-phuc-hop-50-tang-cao-nhat-ha-dong-a760"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/11/22/image003-custom-1637583664.jpg" align="left" hspace="5" /></a>Sáng 21/11/2021, Công ty CP Xây dựng An Phú Gia (APGCons) đã tổ chức lễ khởi công gói thầu thuộc Trung tâm thương mại – văn phòng – chung cư cao tầng Hesco tại quận Hà Đông (Hà Nội). Lễ khởi công đánh dấu cột mốc quan trọng trong hành trình “Bắc tiến" đầy tâm huyết của APGCons. ',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
    },
    pubDate: "2021-11-22 19:22:12",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/11/22/image003-custom-1637583664.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Những khu đô thị tỷ đô kỳ vọng thay đổi diện mạo các vùng đất",
    },
    link:
      "https://toancanhbatdongsan.com.vn/khu-do-thi-ty-do-thay-doi-bo-mat-viet-nam-a692",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/khu-do-thi-ty-do-thay-doi-bo-mat-viet-nam-a692",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/khu-do-thi-ty-do-thay-doi-bo-mat-viet-nam-a692"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/10/26/merryland-quy-nhon-hung-thinh-1635216156.jpg" align="left" hspace="5" /></a>Những khu đô thị có mức đầu tư hàng tỷ đô, thời gian phát triển hàng thập kỷ và mang lại sức sống cho những vùng đất vốn nhiều tiềm năng như Quy Nhơn, Hạ Long, Hưng Yên.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
    },
    pubDate: "2021-10-26 09:51:41",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/10/26/merryland-quy-nhon-hung-thinh-1635216156.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "TS. Đinh Thế Hiển: Bất động sản ven biển Quy Nhơn trở mình thức giấc",
    },
    link:
      "https://toancanhbatdongsan.com.vn/ts-dinh-the-hien-bat-dong-san-ven-bien-quy-nhon-tro-minh-thuc-giac-a338",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/ts-dinh-the-hien-bat-dong-san-ven-bien-quy-nhon-tro-minh-thuc-giac-a338",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/ts-dinh-the-hien-bat-dong-san-ven-bien-quy-nhon-tro-minh-thuc-giac-a338"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/07/05/bai-bien-quy-nhon-1625459155.jpg" align="left" hspace="5" /></a>Từ vùng đất ngủ quên, Quy Nhơn đã trở thành thị trường bất động sản ven biển mới nổi tại miền Trung nhờ khả năng khai thác du lịch tăng mạnh trong khi giá đất còn “mềm”.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Hà Như",
    },
    pubDate: "2021-07-05 11:33:31",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/07/05/bai-bien-quy-nhon-1625459155.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Top 3 dự án căn hộ đắt nhất Sài Gòn bây giờ ra sao: One Central Saigon",
    },
    link:
      "https://toancanhbatdongsan.com.vn/top-3-du-an-can-ho-dat-nhat-sai-gon-bay-gio-ra-sao-one-central-saigon-a329",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/top-3-du-an-can-ho-dat-nhat-sai-gon-bay-gio-ra-sao-one-central-saigon-a329",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/top-3-du-an-can-ho-dat-nhat-sai-gon-bay-gio-ra-sao-one-central-saigon-a329"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/07/02/one-central-saigon-1-resize-1625218882.jpg" align="left" hspace="5" /></a>One Central Saigon đang được xem là dự án đứng đầu danh sách căn hộ siêu sang tại TP.HCM về mức giá đắt đỏ, với giá bán được cho là lên đến 25.000 USD/m2, tương đương với mức giá thấp nhất là 52 tỷ đồng/căn.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Thanh Tâm - Đỗ Khoan",
    },
    pubDate: "2021-07-03 07:00:00",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/07/02/one-central-saigon-1-resize-1625218882.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "The Galleria Residence sẽ được bàn giao từ đầu tháng 7",
    },
    link:
      "https://toancanhbatdongsan.com.vn/the-galleria-residence-se-duoc-ban-giao-tu-dau-thang-7-a311",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/the-galleria-residence-se-duoc-ban-giao-tu-dau-thang-7-a311",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/the-galleria-residence-se-duoc-ban-giao-tu-dau-thang-7-a311"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/30/sk1-1495-1624856811-1625035383.jpeg" align="left" hspace="5" /></a>Đại diện Sơn Kim Land cho biết Tháp The Galleria Residence giai đoạn 1, thuộc dự án Metropole Thủ Thiêm đã hoàn thành giai đoạn 1 và sẽ bàn giao căn hộ cho khách hàng từ đầu tháng 7 năm 2021.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Ngân Khánh",
    },
    pubDate: "2021-06-30 13:44:27",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/30/sk1-1495-1624856811-1625035383.jpeg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Quy Nhơn có thêm 2 dự án căn hộ được mở bán",
    },
    link:
      "https://toancanhbatdongsan.com.vn/quy-nhon-co-them-2-du-an-can-ho-duoc-mo-ban-a223",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/quy-nhon-co-them-2-du-an-can-ho-duoc-mo-ban-a223",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/quy-nhon-co-them-2-du-an-can-ho-duoc-mo-ban-a223"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/thuypham/2021/06/10/vina2-1623294995.jpg" align="left" hspace="5" /></a>Dự án Vina2 Panorama và I - Tower Quy Nhơn (TP. Quy Nhơn) vừa nhận được giấy phép mở bán của Sở Xây dựng tỉnh Bình Định.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Thanh Tâm",
    },
    pubDate: "2021-06-10 18:22:31",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/thuypham/2021/06/10/vina2-1623294995.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Sẽ bàn giao căn hộ The Arena trong tháng 6",
    },
    link:
      "https://toancanhbatdongsan.com.vn/se-ban-giao-can-ho-the-arena-trong-thang-6-a187",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/se-ban-giao-can-ho-the-arena-trong-thang-6-a187",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/se-ban-giao-can-ho-the-arena-trong-thang-6-a187"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/03/the-arena-tai-cam-ranh-1622719154.jpg" align="left" hspace="5" /></a>Khu du lịch Bắc bán đảo Cam Ranh hiện có 40 dự án được cấp giấy chứng nhận đầu tư và The Arena là dự án condotel đầu tiên bàn giao căn hộ cho khách hàng bất chấp những khó khăn do tình hình dịch bệnh Covid-19 kéo dài',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Ngân Khánh",
    },
    pubDate: "2021-06-04 01:22:39",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/06/03/the-arena-tai-cam-ranh-1622719154.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Chủ đầu tư Rose Town cam kết hoàn thành dự án đúng tiến độ",
    },
    link:
      "https://toancanhbatdongsan.com.vn/chu-dau-tu-rose-town-cam-ket-hoan-thanh-du-an-dung-tien-do-a172",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/chu-dau-tu-rose-town-cam-ket-hoan-thanh-du-an-dung-tien-do-a172",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/chu-dau-tu-rose-town-cam-ket-hoan-thanh-du-an-dung-tien-do-a172"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/anhtong/2021/08/29/rose-town-1630173502.jpg" align="left" hspace="5" /></a>Bằng cách tận dụng tối đa mọi nguồn lực có sẵn, chủ đầu tư Xuân Mai Corp cam kết sẽ hoàn thiện dự án Rose Town đúng tiến độ đã cam kết.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Ngân Khánh",
    },
    pubDate: "2021-06-03 14:23:00",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/anhtong/2021/08/29/rose-town-1630173502.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "The Opera Residence giành 3 giải thưởng Bất động sản châu Á - Thái Bình Dương 2021",
    },
    link:
      "https://toancanhbatdongsan.com.vn/the-opera-residence-gianh-3-giai-thuong-bat-dong-san-chau-a-thai-binh-duong-2021-a164",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/the-opera-residence-gianh-3-giai-thuong-bat-dong-san-chau-a-thai-binh-duong-2021-a164",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/the-opera-residence-gianh-3-giai-thuong-bat-dong-san-chau-a-thai-binh-duong-2021-a164"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/05/31/metropole-thu-thiem-cbre-vietnam-1622448944.jpg" align="left" hspace="5" /></a>The Opera Residence, giai đoạn 3 dự án The Metropole Thủ Thiêm do SonKim Land phát triển vừa giành 3 giải thưởng Bất động sản châu Á - Thái Bình Dương 2021.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Ngân Khánh",
    },
    pubDate: "2021-05-31 22:21:36",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/05/31/metropole-thu-thiem-cbre-vietnam-1622448944.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Khai trương Sales Gallery dự án HT Pearl",
    },
    link:
      "https://toancanhbatdongsan.com.vn/khai-truong-sales-gallery-du-an-ht-pearl-a106",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/khai-truong-sales-gallery-du-an-ht-pearl-a106",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/khai-truong-sales-gallery-du-an-ht-pearl-a106"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/24/1-hrpi-1621841675.png" align="left" hspace="5" /></a>HT Pearl là dự án khu căn hộ nằm giáp ranh thành phố Thủ Đức do Nhà Hưng Thịnh làm chủ đầu tư, với giá bán từ 33 triệu đồng/m2.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "H.T",
    },
    pubDate: "2021-05-23 21:38:58",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/24/1-hrpi-1621841675.png",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Mở bán nhà phố Ruby Boutique Residence trong tháng 6",
    },
    link:
      "https://toancanhbatdongsan.com.vn/chuan-bi-mo-ban-12-nha-pho-thuong-mai-ruby-boutique-residence-a104",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/chuan-bi-mo-ban-12-nha-pho-thuong-mai-ruby-boutique-residence-a104",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/chuan-bi-mo-ban-12-nha-pho-thuong-mai-ruby-boutique-residence-a104"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/24/du-an-ruby-boutique-residence-fifa-investment-1621840283.jpg" align="left" hspace="5" /></a>Với giá bán 10,5 - 15 tỷ đồng mỗi căn, Ruby Boutique Residence là một trong số ít dự án nhà phố có hàng mở bán tại TP.HCM hiện tại.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "T.H",
    },
    pubDate: "2021-05-23 21:14:41",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/24/du-an-ruby-boutique-residence-fifa-investment-1621840283.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Dự án Cap Saint Jacques sắp được gia hạn thuê đất",
    },
    link:
      "https://toancanhbatdongsan.com.vn/du-an-cap-saint-jacques-sap-duoc-gia-han-thue-dat-a103",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/du-an-cap-saint-jacques-sap-duoc-gia-han-thue-dat-a103",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/du-an-cap-saint-jacques-sap-duoc-gia-han-thue-dat-a103"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/24/du-an-cap-saint-jaques-tien-do-1621843132.jpg" align="left" hspace="5" /></a>Nếu không được gia hạn, dự án khu phức hợp Cap Saint Jacques (thành phố Vũng Tàu) vừa khởi công năm 2018 chỉ còn thời hạn sử dụng đất 25 năm.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "T.H",
    },
    pubDate: "2021-05-21 20:53:41",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/24/du-an-cap-saint-jaques-tien-do-1621843132.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Giãn tiến độ thanh toán shophouse Meyhomes Capital Phú Quốc",
    },
    link:
      "https://toancanhbatdongsan.com.vn/gian-tien-do-thanh-toan-shophouse-meyhomes-capital-phu-quoc-a102",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/gian-tien-do-thanh-toan-shophouse-meyhomes-capital-phu-quoc-a102",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/gian-tien-do-thanh-toan-shophouse-meyhomes-capital-phu-quoc-a102"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/24/du-an-meyhomes-capital-phu-quoc-1621837481.jpg" align="left" hspace="5" /></a>Trong bối cảnh dịch bệnh bùng phát lần thứ 4, chủ đầu tư Tân Á Đại Thành công bố giãn tiến độ thanh toán các căn shophouse dự án Meyhomes Capital Phú Quốc.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "T.H",
    },
    pubDate: "2021-05-19 20:28:53",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/toan-canh-bat-dong-san/2021/05/24/du-an-meyhomes-capital-phu-quoc-1621837481.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
];
const ListProduct = () => {
  const [visible, setVisible] = useState(false);
  const [info, setInfo] = useState({});

  const onClose = () => {
    setVisible(false);
  };

  return data ? (
    <>
      <List
        style={{ margin: "24px" }}
        itemLayout="vertical"
        grid={{
          gutter: 16,
          xs: 1,
          sm: 2,
          md: 4,
          lg: 4,
          xl: 6,
          xxl: 3,
        }}
        pagination={{
          onChange: (page) => {
            console.log(page);
          },
          pageSize: 6,
        }}
        dataSource={data}
        renderItem={(item) => (
          <>
            <List.Item>
              <Card
                hoverable
                style={{ height: 640 }}
                cover={
                  <img
                    alt={item.title.__cdata}
                    src={item.content._url}
                    style={{
                      height: 480,
                      width: item.content._width,
                      objectFit: "cover",
                    }}
                  />
                }
                onClick={() => {
                  setVisible(true);
                  setInfo(item);
                }}
              >
                <Meta
                  title={item.title.__cdata}
                  description={item.description.__cdata.split("</a>")[1]}
                />
              </Card>
              ,{" "}
            </List.Item>
          </>
        )}
      />
      <DrawerProduct
        visible={visible}
        onClose={onClose}
        title={info?.title?.__cdata}
        overview={info?.description?.__cdata.split("</a>")[1]}
        poster_path={info?.content?._url}
        id={info.id}
        pubDate={info.pubDate}
        link={info.link}
      />
    </>
  ) : (
    <></>
  );
};

export default ListProduct;
