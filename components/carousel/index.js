import { Carousel } from "antd";

const MainCarousel = () => {
  const contentStyle = {
    height: "760px",
    color: "#fff",
    lineHeight: "360px",
    borderRadius: "58px",
  };

  return (
    <Carousel autoplay>
      <div>
        <img src="/images/carousel/cou_2.jpg" style={contentStyle} />{" "}
      </div>{" "}
      <div>
        <img src="/images/carousel/cou_3.jpg" style={contentStyle} />{" "}
      </div>{" "}
      <div>
        <img src="/images/carousel/cou_1.jpg" style={contentStyle} />{" "}
      </div>{" "}
      <div>
        <img src="/images/carousel/cou_4.jpg" style={contentStyle} />{" "}
      </div>{" "}
    </Carousel>
  );
};

export default MainCarousel;
