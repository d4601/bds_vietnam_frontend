import React, { useState } from "react";
import { Card, List } from "antd";
import DrawerProduct from "../../components/drawer-info-product";

const { Meta } = Card;

const data = [
  {
    title: {
      __cdata:
        "Điểm tin bất động sản 31/12: Vinhomes mở bán khu đô thị 2.870ha tại Cần Giờ năm 2023",
    },
    link: "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-3112-a859",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-3112-a859",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-3112-a859"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/31/1-1640919750.jpg" align="left" hspace="5" /></a>Thaiholdings muốn làm cảng vũ trụ du lịch 30.000 tỷ tại Phú Quốc; Vinhomes mở bán khu đô thị 2.870ha tại Cần Giờ năm 2023… là các tin tức đáng chú ý sáng 31/12.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Thanh Hương",
    },
    pubDate: "2021-12-31 10:03:31",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/31/1-1640919750.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Bất động sản 2021: Những dấu ấn đáng nhớ",
    },
    link: "https://toancanhbatdongsan.com.vn/bat-dong-san-2021-2022-a856",
    guid: {
      _isPermaLink: "true",
      __text: "https://toancanhbatdongsan.com.vn/bat-dong-san-2021-2022-a856",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/bat-dong-san-2021-2022-a856"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/30/3-custom-1640831272.jpg" align="left" hspace="5" /></a>Dù trải qua giai đoạn “đóng băng” kéo dài nhưng thị trường địa ốc năm 2021 vẫn để lại nhiều đặc điểm đáng chú ý, trở thành bước đệm đà quan trọng cho những chuyển động trong năm tới.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Y Khải",
    },
    pubDate: "2021-12-30 12:00:57",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/30/3-custom-1640831272.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Điểm tin bất động sản 29/12: Cao tốc Tân Phú - Bảo Lộc dự kiến khởi công năm 2022",
    },
    link: "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2912-a852",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2912-a852",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2912-a852"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/29/1-1640745620.jpg" align="left" hspace="5" /></a>Cao tốc Tân Phú - Bảo Lộc dự kiến khởi công năm 2022; Phó Thủ tướng không cho phép chậm tiến độ sân bay Long Thành… là các tin tức đáng chú ý sáng 2912.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Minh Minh",
    },
    pubDate: "2021-12-29 09:41:08",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/29/1-1640745620.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "7 sự kiện bất động sản nổi bật năm 2021",
    },
    link: "https://toancanhbatdongsan.com.vn/7-su-kien-bat-dong-san-2021-a850",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/7-su-kien-bat-dong-san-2021-a850",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/7-su-kien-bat-dong-san-2021-a850"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/28/toancanhbatdongsan-thumbnail-13-1640651502.jpg" align="left" hspace="5" /></a>Thị trường “nghỉ đông” 4 tháng; tháo chạy khỏi mặt bằng phố Tây; người người livestream bán nhà… là các sự kiện bất động sản đáng nhớ năm 2021.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Nguyễn Đăng",
    },
    pubDate: "2021-12-28 18:00:15",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/28/toancanhbatdongsan-thumbnail-13-1640651502.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Colliers Vietnam: Châu Đức sẽ thành 'điểm nóng' BĐS công nghiệp",
    },
    link:
      "https://toancanhbatdongsan.com.vn/bat-dong-san-cong-nghiep-chau-duc-a849",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/bat-dong-san-cong-nghiep-chau-duc-a849",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/bat-dong-san-cong-nghiep-chau-duc-a849"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/27/bds-kcn-sonadezi-chau-duc-custom-1640598261.jpg" align="left" hspace="5" /></a>Huyện Châu Đức nhiều khả năng sẽ là cái tên được nhắc đến nhiều với tư cách là khu vực có sự tăng trưởng mạnh mẽ về diện tích các khu công nghiệp mới tại Bà Rịa - Vũng Tàu.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
    },
    pubDate: "2021-12-27 16:49:53",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/27/bds-kcn-sonadezi-chau-duc-custom-1640598261.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Điểm tin bất động sản 27/12: Sắp đánh thuế nhà ở để hạn chế đầu cơ?",
    },
    link: "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2712-a845",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2712-a845",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2712-a845"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/27/1-1640572538.jpg" align="left" hspace="5" /></a>Bộ Tài chính nghiên cứu bổ sung thuế BĐS hạn chế đầu cơ; Gamuda muốn thâu tóm nhiều quỹ đất ở Việt Nam… là các tin tức đáng chú ý sáng 27/12.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Bình Minh",
    },
    pubDate: "2021-12-27 09:36:16",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/27/1-1640572538.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "2021 - một năm  kiệt sức của bất động sản nghỉ dưỡng",
    },
    link:
      "https://toancanhbatdongsan.com.vn/2021-mot-nam-bat-dong-san-nghi-duong-kiet-suc-a844",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/2021-mot-nam-bat-dong-san-nghi-duong-kiet-suc-a844",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/2021-mot-nam-bat-dong-san-nghi-duong-kiet-suc-a844"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/24/dau-tu-bat-dong-san-nghi-duong-chuyen-gia-khuyen-than-trong-voi-mot-noi-1635149387-1640328117.jpg" align="left" hspace="5" /></a>Chịu ảnh hưởng nặng nề của dịch Covid-19, thị trường bất động sản nghỉ dưỡng là phân khúc có tình hình kinh doanh ảm đạm trong năm 2021.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
    },
    pubDate: "2021-12-24 13:46:25",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/24/dau-tu-bat-dong-san-nghi-duong-chuyen-gia-khuyen-than-trong-voi-mot-noi-1635149387-1640328117.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Sắp hoàn thành tuyến phố giáo dục – thương mại – y tế đầu tiên tại Bàu Bàng",
    },
    link:
      "https://toancanhbatdongsan.com.vn/tuyen-pho-giao-duc-thuong-mai-y-te-bau-bang-a841",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/tuyen-pho-giao-duc-thuong-mai-y-te-bau-bang-a841",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/tuyen-pho-giao-duc-thuong-mai-y-te-bau-bang-a841"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/24/khong-gian-song-xanh-min-1640314530.jpg" align="left" hspace="5" /></a>Tại huyện Bàu Bàng sẽ lần đầu tiên có mặt tuyến phố đa tiện ích phức hợp, quy tụ loạt trường học đa cấp 14.000m2, công viên 1,2 ha và trung tâm y tế hơn 2.800m2.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Như Bình",
    },
    pubDate: "2021-12-24 12:00:25",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/24/khong-gian-song-xanh-min-1640314530.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Điểm tin bất động sản 24/12: Bình Dương mở rộng quốc lộ 13; Novaland muốn huy động 6.000 tỷ đồng trái phiếu",
    },
    link:
      "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2412-binh-duong-mo-rong-quoc-lo-13-novaland-muon-huy-dong-6000-ty-dong-trai-phieu-a843",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2412-binh-duong-mo-rong-quoc-lo-13-novaland-muon-huy-dong-6000-ty-dong-trai-phieu-a843",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2412-binh-duong-mo-rong-quoc-lo-13-novaland-muon-huy-dong-6000-ty-dong-trai-phieu-a843"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/24/copy-of-gray-light-professional-static-announcement-general-news-twitter-post-2-1640315504.jpg" align="left" hspace="5" /></a>Bình Dương mở rộng quốc lộ 13 lên 8 làn xe; Novaland muốn huy động gần 6.000 tỷ đồng từ trái phiếu… là các tin bất động sản đáng chú ý sáng 24/12.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
    },
    pubDate: "2021-12-24 10:12:50",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/24/copy-of-gray-light-professional-static-announcement-general-news-twitter-post-2-1640315504.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Đăk Nông -  điểm sáng cho giới đầu tư năm 2022",
    },
    link:
      "https://toancanhbatdongsan.com.vn/daknong-diem-sang-gioi-dau-tu-2022-a838",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/daknong-diem-sang-gioi-dau-tu-2022-a838",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/daknong-diem-sang-gioi-dau-tu-2022-a838"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/23/cong-vien-dia-chat-toan-cau-daknong-min-1640226715.jpg" align="left" hspace="5" /></a>Đăk Nông từ một vùng đất còn nhiều khó khăn, hiện nay dần trở thành mục tiêu của nhiều nhà đầu tư trong và ngoài nước bởi những thay đổi về hạ tầng công nghệ, nguồn nhân lực và quan trọng nhất là những chính sách ưu đãi thu hút đầu tư.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Bảo An",
    },
    pubDate: "2021-12-23 09:33:40",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/23/cong-vien-dia-chat-toan-cau-daknong-min-1640226715.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Điểm tin bất động sản 22/12: Thị trường “nóng” trở lại; Liên danh có Hưng Thịnh lập quy hoạch khu vực 15.000 ha tại Lâm Đồng",
    },
    link: "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2212-a836",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2212-a836",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2212-a836"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/22/copy-of-gray-light-professional-static-announcement-general-news-twitter-post-1-1640145783.jpg" align="left" hspace="5" /></a>Thị trường “nóng” trở lại; Liên danh có Hưng Thịnh lập quy hoạch khu vực 15.000 ha tại Lâm Đồng… là các tin bất động sản đáng chú ý sáng 22/12.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
    },
    pubDate: "2021-12-22 12:03:12",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/22/copy-of-gray-light-professional-static-announcement-general-news-twitter-post-1-1640145783.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Kịch bản nào cho ngành bất động sản năm 2022?",
    },
    link:
      "https://toancanhbatdongsan.com.vn/kich-ban-nao-cho-nganh-bat-dong-san-nam-2022-a834",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/kich-ban-nao-cho-nganh-bat-dong-san-nam-2022-a834",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/kich-ban-nao-cho-nganh-bat-dong-san-nam-2022-a834"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/22/dji-0426-6505-1639368018-custom-1640139618.jpg" align="left" hspace="5" /></a>Cả VNDirect và MBKE đều có góc nhìn lạc quan khi bàn về kịch bản bất động sản năm 2022 dựa vào tình hình vĩ mô có nhiều dư địa tăng trưởng và động thái gỡ nút thắt pháp lý.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Y Khải",
    },
    pubDate: "2021-12-22 09:21:41",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/22/dji-0426-6505-1639368018-custom-1640139618.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Thanh Hóa trở thành “điểm nóng” BĐS công nghiệp ở phía Bắc",
    },
    link:
      "https://toancanhbatdongsan.com.vn/thanh-hoa-tro-thanh-diem-nong-bds-cong-nghiep-o-phia-bac-a831",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/thanh-hoa-tro-thanh-diem-nong-bds-cong-nghiep-o-phia-bac-a831",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/thanh-hoa-tro-thanh-diem-nong-bds-cong-nghiep-o-phia-bac-a831"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/21/dji-0966-jpg-1631092775-9720-1631092849-custom-1640053853.jpg" align="left" hspace="5" /></a>Lý do quan trọng khiến Thanh Hóa có lợi thế lớn về bất động sản công nghiệp bao gồm quỹ đất lớn, nguồn nhân lực dồi dào, cơ sở hạ tầng tốt và liên tục được đầu tư phát triển.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
    },
    pubDate: "2021-12-21 09:32:56",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/21/dji-0966-jpg-1631092775-9720-1631092849-custom-1640053853.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "‘Fintech là anh, proptech là em’",
    },
    link:
      "https://toancanhbatdongsan.com.vn/fintech-la-anh-proptech-la-em-a828",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/fintech-la-anh-proptech-la-em-a828",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/fintech-la-anh-proptech-la-em-a828"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/20/265982177-434746078100238-8397081130837421759-n-1639972619.jpg" align="left" hspace="5" /></a>Theo chuyên gia từ quỹ đầu tư, proptech - công nghệ bất động sản tuy đi sau nhưng sẽ sớm đạt các thành tựu như ‘người anh em’ fintech – công nghệ tài chính. ',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
    },
    pubDate: "2021-12-20 11:09:53",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/20/265982177-434746078100238-8397081130837421759-n-1639972619.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Điểm tin bất động sản 20/12: Đắk Nông chỉ đạo thanh tra đột xuất; Novaland có tân Giám đốc Tài chính",
    },
    link:
      "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2012-dak-nong-chi-dao-thanh-tra-dot-xuat-novaland-co-tan-giam-doc-tai-chinh-a827",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2012-dak-nong-chi-dao-thanh-tra-dot-xuat-novaland-co-tan-giam-doc-tai-chinh-a827",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-2012-dak-nong-chi-dao-thanh-tra-dot-xuat-novaland-co-tan-giam-doc-tai-chinh-a827"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/20/1-1639967065.jpg" align="left" hspace="5" /></a>Đắk Nông chỉ đạo thanh tra đột xuất; Novaland có tân Giám đốc Tài chính... là các tin tức bất động sản đáng chú ý.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
    },
    pubDate: "2021-12-20 09:26:38",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/20/1-1639967065.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Vì sao dự án của chủ đầu tư ngoại cứ ra hàng là hết?        ",
    },
    link:
      "https://toancanhbatdongsan.com.vn/vi-sao-du-an-cua-chu-dau-tu-ngoai-cu-ra-hang-la-het-a825",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/vi-sao-du-an-cua-chu-dau-tu-ngoai-cu-ra-hang-la-het-a825",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/vi-sao-du-an-cua-chu-dau-tu-ngoai-cu-ra-hang-la-het-a825"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/17/cau-anh-sap-hoang-hon-custom-1639728967.jpg" align="left" hspace="5" /></a>Nguồn cung khan hiếm, chất lượng thi công vượt trội, đúng tiến độ, quy trình pháp lý chuẩn xác… là những lý do khiến các dự án của các chủ đầu tư ngoại luôn “cháy hàng".',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Minh Minh",
    },
    pubDate: "2021-12-17 18:16:11",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/17/cau-anh-sap-hoang-hon-custom-1639728967.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Điểm tin bất động sản 17/12: Trái phiếu doanh nghiệp BĐS vẫn hút khách",
    },
    link: "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-1712-a824",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-1712-a824",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-1712-a824"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/17/1-1639711527.jpg" align="left" hspace="5" /></a>Bình Định duyệt quy hoạch khu đô thị 800 tỷ của May-Diêm Sài Gòn; Trái phiếu doanh nghiệp BĐS vẫn hút khách... là các tin tức đáng chú ý sáng 17/12.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Hưng Lợi",
    },
    pubDate: "2021-12-17 10:26:07",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/17/1-1639711527.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata: "Tà Đùng - Từ đại ngàn hoang sơ thành ‘nàng thơ’ nghỉ dưỡng mới",
    },
    link: "https://toancanhbatdongsan.com.vn/khai-pha-tiem-nang-ta-dung-a820",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/khai-pha-tiem-nang-ta-dung-a820",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/khai-pha-tiem-nang-ta-dung-a820"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/16/ho-ta-dung-min-1639623672.jpg" align="left" hspace="5" /></a>Trong lần đi săn ảnh ở Tà Đùng, ông Đông vô tình tìm thấy chốn bồng lai tiên cảnh và quyết định xây Tà Đùng Top View Homestay. Hiện nay homestay đã nổi tiếng nhất tỉnh Đắk Nông và thu về tiền tỷ mỗi tháng.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
    },
    pubDate: "2021-12-17 09:05:02",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/2021/12/16/ho-ta-dung-min-1639623672.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Toàn cảnh bất động sản Long An: sức nóng từ bất động sản công nghiệp",
    },
    link:
      "https://toancanhbatdongsan.com.vn/toan-canh-bat-dong-san-long-an-suc-nong-tu-bat-dong-san-cong-nghiep-a821",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/toan-canh-bat-dong-san-long-an-suc-nong-tu-bat-dong-san-cong-nghiep-a821",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/toan-canh-bat-dong-san-long-an-suc-nong-tu-bat-dong-san-cong-nghiep-a821"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/16/khu-cong-nghiep-duc-hoa-long-an-1-custom-1639624967.jpeg" align="left" hspace="5" /></a>Với quỹ đất rộng, giá thành tương đối thấp, cơ sở hạ tầng giao thông ngày càng được nâng cấp, Long An đang trở thành điểm đến ngày càng được săn đón của giới đầu tư và người mua thực. ',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
      __text: "Lý Võ",
    },
    pubDate: "2021-12-16 10:23:13",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/16/khu-cong-nghiep-duc-hoa-long-an-1-custom-1639624967.jpeg",
      _width: "120",
      __prefix: "media",
    },
  },
  {
    title: {
      __cdata:
        "Điểm tin bất động sản 15/12: Vũng Áng, Ninh Hòa sốt đất; Ecopark muốn xây đô thị 49ha tại Đà Lạt",
    },
    link: "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-1512-a817",
    guid: {
      _isPermaLink: "true",
      __text:
        "https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-1512-a817",
    },
    description: {
      __cdata:
        '<a href="https://toancanhbatdongsan.com.vn/diem-tin-bat-dong-san-1512-a817"><img width="180px" border="0" src="https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/15/1-1639536027.jpg" align="left" hspace="5" /></a>Vũng Áng, Ninh Hòa sốt đất; Ecopark muốn xây đô thị 49ha tại Đà Lạt... là các tin tức đáng chú ý sáng 15/12.',
    },
    creator: {
      "_xmlns:dc": "http://purl.org/dc/elements/1.1/",
      __prefix: "dc",
    },
    pubDate: "2021-12-15 09:41:04",
    content: {
      _url:
        "https://toancanhbatdongsan.com.vn/zoom/480x360/uploads/images/blog/tuannhu/2021/12/15/1-1639536027.jpg",
      _width: "120",
      __prefix: "media",
    },
  },
];

const ProductsList = () => {
  const [visible, setVisible] = useState(false);
  const [info, setInfo] = useState();

  const onClose = () => {
    setVisible(false);
  };

  return (
    <>
      <List
        itemLayout="vertical"
        grid={{
          gutter: 16,
          xs: 1,
          sm: 2,
          md: 4,
          lg: 4,
          xl: 6,
          xxl: 3,
        }}
        style={{ margin: "24px" }}
        dataSource={data}
        pagination={{
          onChange: (page) => {
            console.log(page);
          },
          pageSize: 6,
        }}
        renderItem={(item) => (
          <List.Item>
            <Card
              hoverable
              style={{ height: 640 }}
              cover={
                <img
                  alt={item.title.__cdata}
                  src={item.content._url}
                  style={{
                    height: 480,
                    width: item.content._width,
                    objectFit: "cover",
                  }}
                />
              }
              onClick={() => {
                setVisible(true);
                setInfo(item);
              }}
            >
              <Meta
                title={item.title.__cdata}
                description={item.description.__cdata.split("</a>")[1]}
              />
            </Card>
            ,{" "}
          </List.Item>
        )}
      />
      <DrawerProduct
        visible={visible}
        onClose={onClose}
        title={info?.title?.__cdata}
        overview={info?.description?.__cdata.split("</a>")[1]}
        poster_path={info?.content?._url}
        id={info?.id}
        pubDate={info?.pubDate}
        link={info?.link}
      />
    </>
  );
};

export default ProductsList;
