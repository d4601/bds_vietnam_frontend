import React from "react";
import { Steps, Row } from "antd";
import Link from "next/link";
import { inject, observer } from "mobx-react";
import { routerLink } from "../../router";

const { Step } = Steps;

const StepRender = ({ currentStatus = 1, authStore }) => {
  const { isAuthenticated, cartAmount } = authStore;

  if (isAuthenticated) currentStatus = 2;

  return (
    <Row justify="center" style={{ margin: "80px" }}>
      <Steps size="small" current={currentStatus}>
        {!cartAmount ? (
          <Step status="wait" title="Đọc tin tức" />
        ) : (
          <Step status="finish" title="Đọc tin tức" />
        )}
        {!isAuthenticated ? (
          <Step
            status="wait"
            title={
              <Link {...routerLink.auth.login.get().linkProps()}>
                Để lại thông tin tư vấn
              </Link>
            }
          />
        ) : (
          <Step status="finish" title="Để lại thông tin tư vấn" />
        )}
      </Steps>
    </Row>
  );
};

export default inject("authStore")(observer(StepRender));
