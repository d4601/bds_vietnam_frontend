import styled from 'styled-components'

const S4Clearfix = styled.div`
  display: block;

  width: ${props => typeof props.w === 'string' ? props.w : props.w + 'px'};
  height: ${props => typeof props.h === 'string' ? props.h : props.h + 'px'};
`

const Clearfix = ({ width = '100%', height = 16 }) => {
  return (
    <S4Clearfix
      w={width}
      h={height}
    />
  )
}

export default Clearfix
