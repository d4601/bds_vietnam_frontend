import React from 'react'
import { Typography, Row, Col, Tag, Statistic } from 'antd'

const { Title } = Typography
const { Countdown } = Statistic

const SimpleHeading = ({
  marginTop = 50,
  marginBottom = 30,
  marginLeft = 40,
  textTransform = 'none',
  level,
  title,
  description,
  countdown,
  tags
}) => {
  return (
    <Row justify='start'>
      <Col style={{ marginTop, marginBottom, marginLeft }} span={30}>
        <small style={{ textTransform, color: 'Highlight' }}>- {description}</small>
        <Title style={{ textTransform, border: '1px' }} level={level}>
          {title}
        </Title>
      </Col>

      {tags
        ? (
          <Col style={{ marginTop: 70, marginBottom: 35, marginLeft: 32 }} span={70}>
            {tags.map((tag) => {
              return (<Tag key={tag.id} color='#8caabe'>#{tag.name}</Tag>)
            })}
          </Col>
        ) : (<></>)}
      {
        countdown
          ? (
            <Col style={{ marginTop: 65, marginBottom: 35, marginLeft: 32 }} span={70}>
              <Countdown value={countdown} onFinish={() => {}} />
            </Col>
          ) : (
            <></>
          )
      }
    </Row>
  )
}

export default SimpleHeading
