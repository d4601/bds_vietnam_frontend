import HeaderMeta from './header'
import MainNav from './navbar'
import MainFooter from './footer'
import { Layout } from 'antd'

const { Content } = Layout

const MainLayout = ({ children, title, currentTab }) => {
  return (
    <>
      <HeaderMeta title={title} />
      <Layout className='layout'>
        <MainNav currentTab={currentTab} />
        <Content style={{ minHeight: '500px', margin: '80px' }}>{children}</Content>
        <MainFooter />
      </Layout>
    </>
  )
}

export default MainLayout
