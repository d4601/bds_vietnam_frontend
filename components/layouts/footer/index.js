import React from "react";
import { Container, Row, Col } from "styled-bootstrap-grid";

import { S4Footer, Line, Contact, Copyright } from "../styles";

const Footer = () => {
  return (
    <S4Footer>
      <Container>
        {/* <Line /> */}

        <Row>
          <Col span={24} xl={12}>
            <Copyright>Copyright @2021. UIT Web</Copyright>
          </Col>
          {/* <Col span={24} xl={12}>
            <Contact>
              <span className="contact-item">
                <i className="far fa-envelope" />
                .vn
              </span>
            </Contact>
          </Col> */}
        </Row>
      </Container>
    </S4Footer>
  );
};

export default Footer;
