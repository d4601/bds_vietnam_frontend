import React, { useState } from "react";
import { AutoComplete } from "antd";
import DrawerProduct from "../../drawer-info-product";

const API_KEY = "4a5a0bdfb16d0651a1fd2bca8473ef6f";

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

const AutoCompleteComp = () => {
  const [text, setText] = useState("");
  const [options, setOptions] = useState([{ value: "" }]);
  const [visible, setVisible] = useState(false);
  const [info, setInfo] = useState({
    title: "",
    overview: "",
    poster_path: "",
    id: "",
    vote_average: "",
    vote_count: "",
  });
  const [data, setData] = useState([]);

  const onClose = () => {
    setVisible(false);
  };

  const getMoviesDB = async (searchTerm) => {
    const url = `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&query=${searchTerm}`;
    const call_api = await fetch(url);
    const data = await call_api.json();

    setOptions(
      data &&
        data.results.map((item) => {
          return { value: item.title, data: item };
        })
    );
    setData(data);
  };

  const handleOnSelect = (item) => {
    setVisible(true);
    const findInfo = data && data.results.find((i) => i.title == item);
    setInfo(findInfo);
  };

  const handleSearchInput = (data) => {
    data && getMoviesDB(data);
  };

  const handleChangeInput = (data) => {
    setTimeout(setText(data), 0);
  };

  return (
    <>
      <AutoComplete
        value={text}
        placeholder="Search for everything..."
        size="medium"
        style={{ width: 400 }}
        onSearch={handleSearchInput}
        onChange={handleChangeInput}
        onSelect={handleOnSelect}
        options={options}
      />
      <DrawerProduct
        visible={visible}
        onClose={onClose}
        title={info.title}
        overview={info.overview}
        poster_path={info.poster_path}
        id={info.id}
        vote_average={info.vote_average}
        vote_count={info.vote_count}
        disabled={getRandomInt(2) < 1 ? true : false}
      />
    </>
  );
};

export default AutoCompleteComp;
