import NumberInput from 'components/form/number-input'
import createFormikField from 'hoc/formik-field'

const NumberInputField = createFormikField(NumberInput)

export default NumberInputField
