import DateRangePicker from '../../form/date-range-picker'
import createFormikField from 'hoc/formik-field'

const DateRangePickerField = createFormikField(DateRangePicker)

export default DateRangePickerField
