import LocationSearchInput from 'components/form/location-search-input'
import createFormikField from 'hoc/formik-field'

const LocationInputField = createFormikField(LocationSearchInput)

export default LocationInputField
