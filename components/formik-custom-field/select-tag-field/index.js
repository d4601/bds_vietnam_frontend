import Select from '../../form/select'
import createFormikField from 'hoc/formik-field'

const SelectTagField = createFormikField(Select.Tag)

export default SelectTagField
